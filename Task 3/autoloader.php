<?php

spl_autoload_register(function ($classname) {
    $filename = str_replace('\\', '/', $classname) . ".php";
    include($filename);
});
