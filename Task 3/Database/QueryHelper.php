<?php

namespace Database;

use Exception;
use Factory\ProductFactory;
use PDO;
use Products\Product;

class RowsNotFoundException extends Exception
{
}

class QueryHelper
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var ProductFactory
     */
    private $productFactory;

    /**
     * @param PDO $db
     * @param ProductFactory $productFactory
     */
    public function __construct(\PDO $db, ProductFactory $productFactory)
    {
        $this->pdo = $db;
        $this->productFactory = $productFactory;
    }

    /**
     * Returns product object with all specified attributes by its id
     * @param int $id
     * @return Product
     * @throws RowsNotFoundException
     */
    public function getProduct(int $id) : Product
    {
        $query = $this->pdo->prepare("SELECT p.product_id, p.title, p.price, t.type_id FROM Products p
            LEFT JOIN Types t ON p.type_id = t.type_id
            WHERE p.product_id = :product_id");

        $query->execute([
            'product_id' => $id
        ]);


        if ($query->rowCount() === 0) {
            throw new RowsNotFoundException('Cannot find product with id: ' . $id);
        }

        $result = $query->fetch(PDO::FETCH_ASSOC);
        $attributes = $this->getProductAttributes($result['product_id']);
        $product = $this->productFactory->create($result['type_id'], $result, $attributes);

        return $product;
    }

    /**
     * Returns array of products of specified type (Furniture or Disks)
     * @param int $productType
     * @return mixed
     * @throws RowsNotFoundException
     */
    public function getAllProducts(int $productType) : array
    {
        $query = $this->pdo->prepare("SELECT p.product_id, p.title, p.price FROM Products p
            WHERE p.type_id = :type_id");

        $query->execute([
            'type_id' => $productType
        ]);

        if ($query->rowCount() === 0) {
            throw new RowsNotFoundException('Cannot find any products of category: ' . $productType);
        }

        $products = [];
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $attributes = $this->getProductAttributes($row['product_id']);

            $products[] = $this->productFactory->create($productType, $row, $attributes);
        }

        return $products;
    }

    /**
     * Returns product specific attributes (e.g. : capacity for Disk product, dimensions for Furniture product, etc.)
     * @param int $id
     * @return array
     * @throws RowsNotFoundException
     */
    public function getProductAttributes(int $id) : array
    {
        $query = $this->pdo->prepare("SELECT a.name, p.value FROM Product_attributes p
            INNER JOIN Attributes a ON p.attribute_id = a.attribute_id
            WHERE p.product_id = :product_id");

        $query->execute([
            'product_id' => $id
        ]);


        if ($query->rowCount() === 0) {
            throw new RowsNotFoundException('Cannot fetch attributes for product id: ' . $id);
        }

        $attributes = [];

        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
            $attributes[$row['name']] = $row['value'];
        }

        return $attributes;
    }
}