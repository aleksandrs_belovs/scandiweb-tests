<?php

namespace Factory;

use Products\{
    Disk, Furniture, Product
};
use Exception;

class ProductFactory
{
    /**
     * Creates an object of type Furniture or Disk
     * @param int $productType
     * @param array $baseAttributes
     * @param array $additionalAttributes
     * @return \Product
     * @throws Exception
     */
    public function create(int $productType, array $baseAttributes, array $additionalAttributes) : Product
    {
        switch ($productType) {
            case TYPE_DISK:
                $product = new Disk($baseAttributes['title'], (float)$baseAttributes['price'], $additionalAttributes['capacity'], $additionalAttributes['manufacturer']);
                break;
            case TYPE_FURNITURE:
                $product = new Furniture($baseAttributes['title'], (float)$baseAttributes['price'], $additionalAttributes['dimensions'], $additionalAttributes['material']);
                break;
            default:
                throw new Exception('Cannot create product of type: ' . $productType);

        }

        return $product;
    }
}