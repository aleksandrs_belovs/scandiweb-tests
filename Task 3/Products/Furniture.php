<?php

namespace Products;

class Furniture extends Product
{
    /**
     * Dimensions of furniture object
     * @var string
     */
    private $dimensions;

    /**
     * Material of furniture object
     * @var string
     */
    private $material;

    /**
     * Allowed materials for furniture object
     * @var array
     */
    private $allowedMaterials = ["plastic", "wood"];

    /**
     * @param string $title
     * @param float  $price
     * @param string $dimensions
     * @param string $material
     */
    public function __construct(string $title, float $price, string $dimensions, string $material)
    {
        parent::__construct($title, $price);

        $this->setMaterial($material);
        $this->setDimensions($dimensions);
    }

    /**
     * Returns information about corresponding object as string
     * @return string
     */
    public function getAllAttributesAsString() : string
    {
        $productAttributes = parent::getAllAttributesAsString();

        return $productAttributes . "\ndimensions: " . $this->getDimensions() . "\nmaterial: " . $this->getMaterial() . "\n";

    }

    /**
     * @return string
     */
    public function getDimensions() : string
    {
        return $this->dimensions;
    }

    /**
     * @return string
     */
    public function getMaterial() : string
    {
        return $this->material;
    }

    /**
     * @param string $dimensions
     * @return Furniture
     */
    private function setDimensions(string $dimensions) : self
    {
        $pattern = "/^([0-9]|[1-9][0-9]*)x([0-9]|[1-9][0-9]*)x([0-9]|[1-9][0-9]*)$/";
        if (preg_match($pattern, $dimensions)) {
            $this->dimensions = $dimensions;
        } else {
            throw new \InvalidArgumentException('Invalid dimension value');
        }

        return $this;
    }

    /**
     * @param string $material
     * @return Furniture
     */
    private function setMaterial(string $material) : self
    {
        if (in_array($material, $this->allowedMaterials)) {
            $this->material = $material;
        } else {
            throw new \InvalidArgumentException('Invalid material value');
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getAllowedMaterials() : array
    {
        return $this->allowedMaterials;
    }
}
