<?php

namespace Products;

class Disk extends Product
{
    /**
     * @var int
     */
    private $capacity;

    /**
     * @var string
     */
    private $manufacturer;


    /**
     * Disk constructor.
     * @param string $title
     * @param float $price
     * @param int $capacity
     * @param string $manufacturer
     */
    public function __construct(string $title, float $price, int $capacity, string $manufacturer)
    {
        parent::__construct($title, $price);

        $this->capacity = $capacity;
        $this->manufacturer = $manufacturer;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title . ' ' . $this->capacity . 'MB';
    }

    /**
     * @return string
     */
    public function getAllAttributesAsString() : string
    {
        $productAttributes = parent::getAllAttributesAsString();

        return $productAttributes . "\ncapacity: " . $this->getCapacity() . "\nmanufacturer: " . $this->getManufacturer() . "\n";
    }

    /**
     * @return string
     */
    public function getCapacity() : string
    {
        return $this->capacity . 'MB';
    }

    /**
     * @return string
     */
    public function getManufacturer() : string
    {
        return $this->manufacturer;
    }
}
