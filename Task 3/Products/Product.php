<?php

namespace Products;

class Product
{
    /**
     * Title of the product
     * @var string
     */
    protected $title;

    /**
     * Price of the product
     * @var string
     */
    protected $price;

    /**
     * @param string $title
     * @param float $price
     */
    public function __construct(string $title, float $price)
    {
        $this->title = $title;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getTitle() : string
    {
        return $this->title;
    }

    /**
     * @return float
     */
    public function getPrice() : float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getAllAttributesAsString() : string
    {
        return 'title: ' . $this->getTitle() . "\nprice: " . $this->getPrice();
    }
}