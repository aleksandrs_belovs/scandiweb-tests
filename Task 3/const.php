<?php

// Database specific constants
define('DB_HOST', 'localhost' );
define('DB_USERNAME', '' );
define('DB_PASSWORD', '');
define('DB_NAME', 'catalogue');

// Types of product
define('TYPE_DISK', 1);
define('TYPE_FURNITURE', 2);
