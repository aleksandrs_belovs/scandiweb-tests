<?php

use Database\QueryHelper;
use Factory\ProductFactory;
use Products\Disk;
use Products\Furniture;

require_once('const.php');
require_once('autoloader.php');

// Make new objects of furniture or disk
$furniture = new Furniture('Table', '189.5', '200x100x250', 'plastic');
$disk = new Disk('Audio Disk x52 CD-RW', '2.55', 700, 'TDK');

// Get properties of object: price, material, title, dimensions, all attributes
/*
$furniture->getPrice();
$furniture->getMaterial();
$furniture->getTitle();
$furniture->getDimensions();
$furniture->getAllAttributesAsString();
*/

// Init database connection
$pdo = new PDO(sprintf('mysql:host=%s;dbname=%s', DB_HOST, DB_NAME), DB_USERNAME, DB_PASSWORD);
$query = new QueryHelper($pdo, new ProductFactory());

// Get specific product by id

try {
    print_r($query->getProduct(3));
} catch (\Database\RowsNotFoundException $e) {
    print($e);
}


// Get all products of specified category
/*
try {
    print_r($query->getAllProducts(TYPE_DISK));
} catch (\Database\RowsNotFoundException $e) {
    print($e);
}
*/



