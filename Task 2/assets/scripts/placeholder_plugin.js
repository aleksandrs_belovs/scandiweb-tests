(function($) {

    $.fn.addplaceholder = function () {

        // If placeholders are supported by browser simply return
        //if (document.createElement('input').placeholder !== undefined) return;

        this.each(function() {
            var placeholderText = $(this).data('placeholder');
            $(this)
                .val(placeholderText)
                .addClass('notActive')
                .on({

                    'click': function() {
                        if ($(this).hasClass('notActive')) {
                            // Move cursor to the start of input
                            $(this).caret(0);
                        }
                    },

                    'keydown': function() {
                        var el = $(this);

                        if (el.hasClass('notActive')) {
                            removePlaceholder(el);
                        }
                    },

                    'keyup': function() {
                        var el = $(this);
                        if (el.val().length === 0) {
                            addPlaceholder(el);
                            $(this).caret(0);
                        }
                    }
                });

        });

        function removePlaceholder(el) {
            el.val('');
            el.removeClass('notActive').addClass('active');
        }

        function addPlaceholder(el) {
            el.removeClass('active').addClass('notActive');
            var placeholderText = el.data('placeholder');
            el.val(placeholderText);
        }

        return this;
    };


}(jQuery));